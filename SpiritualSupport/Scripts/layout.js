﻿var layout = {};

$(function () {
    $("[data-id='linkToSectionOnHomePage']").click(function () {
        var destination = $(this).attr("href");
        var pathname = window.location.pathname;

        if (pathname !== "/") {
            console.log($(this).attr("href"));
            window.location.href = "/" + destination;
        }
    });
    
    layout.InitializeDatePickers = function () {
        $(".date-picker")
            .filter(function (i, v) {return $(v).data('daterangepicker') === undefined})
            .each(function (i, v){
                $(v).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: moment().year(),
                    maxYear: parseInt(moment().year(),10),
                    startDate: new Date($(v).val())
                });
            });

        $(".time-picker")
            .filter(function (i, v) {return $(v).data('daterangepicker') === undefined})
            .each(function (i, v){
                $(v).daterangepicker({
                    singleDatePicker: true,
                    timePicker: true,
                    locale: {
                        format: 'hh:mm A'
                    },
                    minYear: moment().year(),
                    maxYear: parseInt(moment().year(),10),
                    startDate: new Date($(v).val())
                }).on('show.daterangepicker', function (ev, picker) {
                    picker.container.find(".calendar-table").hide();
                });
            });
    }
});