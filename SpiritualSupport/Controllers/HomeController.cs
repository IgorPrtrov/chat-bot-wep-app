﻿using System.Web.Mvc;
using SpiritualSupport.Helpers;

namespace SpiritualSupport.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ErrorInform()
        {
            ViewBag.Error = ExceptionLoggerAttribute.ErrorMessages;
            ExceptionLoggerAttribute.ErrorMessages = string.Empty;
            return View();
        }
    }
}