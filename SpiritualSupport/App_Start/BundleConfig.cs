﻿using System.Web.Optimization;

namespace SpiritualSupport
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Bootstrap4/popper.min.js",
                      "~/Scripts/Bootstrap4/bootstrap.min.js",
                      "~/Scripts/Bootstrap4/bootstrap4-toggle.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/layoutJS").Include(
                "~/Scripts/layout.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-bar-rating").Include(
                "~/Scripts/jquery.barrating.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Bootstrap4/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/validation.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/PagedList.css",
                      "~/Content/Bootstrap4/bootstrap4-toggle.min.css"));

            bundles.Add(new StyleBundle("~/Content/css/homePage").Include(
                "~/Content/Styles/home-page.css"));

            bundles.Add(new StyleBundle("~/Content/css/jquery-bar-rating").Include(
                "~/Content/bars-square.css"));
        }
    }
}
