﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SpiritualSupport.Helpers;
using FluentValidation.Mvc;

namespace SpiritualSupport
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FluentValidationModelValidatorProvider.Configure();
            ModelValidatorProviders.Providers.Remove(
                ModelValidatorProviders.Providers.OfType<DataAnnotationsModelValidatorProvider>().First());
        }

        protected void Application_End(object sender, EventArgs e)
        {
            
        }

        protected void Application_Error(object sender, EventArgs e)
        {            
            Exception ex = Server.GetLastError();
            ExceptionLoggerAttribute.ErrorMessages = ex.Message;

            Response.Redirect("/Home/ErrorInform");
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {

        }
    }
}
